package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.printf("Let's play round %d", roundCounter);
            System.out.print("\nYour choice (Rock/Paper/Scissors)? ");

            String myMove = sc.nextLine();
            if (!rpsChoices.contains(myMove)) {
                System.out.printf("I do not understand %s. Could you try again? ", myMove);
                System.out.print("\nYour choice (Rock/Paper/Scissors)? ");
                myMove = sc.nextLine();
            }

            int rand = (int)(Math.random()*3);
            String computerMove = "";
            if (rand == 0) {
                computerMove = "rock";
            }   else if(rand == 1) {
                computerMove = "paper";
            }   else {
                computerMove = "scissors";
            }
            if (myMove.equals(computerMove)) {
                System.out.printf("\nHuman chose %s, computer chose %s. It's a tie!", myMove, computerMove);
                System.out.printf("\nScore: human %d, computer %d", humanScore, computerScore);
            }else if ((myMove.equals("rock") && computerMove.equals("scissors")) || (myMove.equals("scissors"))
                && (computerMove.equals("paper")) || (myMove.equals("paper")) && (computerMove.equals("rock"))) {
                System.out.printf("\nHuman chose %s, computer chose %s. Human wins!", myMove, computerMove);
                humanScore++;
                System.out.printf("\nScore: human %d, computer %d", humanScore, computerScore);
            }else {
                System.out.printf("\nHuman chose %s, computer chose %s. Computer wins!", myMove, computerMove);
                computerScore++;
                System.out.printf("\nScore: human %d, computer %d", humanScore, computerScore);
            }
            roundCounter++;
            System.out.print("\nDo you wish to continue playing? (y/n)? \n");
            String newRound = sc.nextLine();
            if (newRound.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
